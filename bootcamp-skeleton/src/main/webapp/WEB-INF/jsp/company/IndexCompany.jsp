<div class="row">
<div class="col-sm-12">
	
	<!-- /.box-header -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">List Company</h3>
		</div>
		
		<!-- body -->
		<div>
			<button type="button" class="btn btn-info pull-right"
				data-toggle="modal" data-target="#addModal">Add</button>
		</div>
		
		<!-- form start -->
		<form class="form-inline" style="padding-top: 30px" >
			<div class="form-group ">
				<div class="col-sm-5">
					<input list="code" name="code"
						placeholder="- Select Company Code -">
					<datalist id="code">
						<option value="Internet Explorer">
						<option value="Firefox">
						<option value="Chrome">
						<option value="Opera">
						<option value="Safari">
					</datalist>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5">
					<input list="name" name="name"
						placeholder="- Select Company Name -">
					<datalist id="name">
						<datalist id="name">
							<option value="Internet Explorer">
							<option value="Firefox">
							<option value="Chrome">
							<option value="Opera">
							<option value="Safari">
						</datalist>
						</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5">
					<input type="date" name="date" placeholder="- Date -"> </input>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-5">
					<input type="text" name="createdBy" placeholder="Created By">
					</input>
				</div>
			</div>

			<div class="form-group">
			
				<div class="col-sm-5">
					<button type="button" class="btn btn-info"
						id="btnSearch">Search</button>
				</div>
			</div>
			<table id="example" class="table table-stripe">
			<th>
			<td>
			</th>
			</table>
			
		</form>
	</div>
</div>
	<%@include file="ModalCompany.jsp"%>
	<script>
		$(function() {
			formCode();
			listCompany();
			$('#btnSave').click(function() {
				insertCompany();
				$('#example').DataTable().destroy();
				$('#example').empty();
				
				listCompany();
			});
			
		})

		function insertCompany() {
			$.ajax({
				type : "POST",
				url : contextName + "/company/insert.json",
				data : {
					companyCode : $('#companyCode').val(),
					companyName : $('#companyName').val(),
					email : $('#email').val(),
					phone : $('#phone').val(),
					address : $('#address').val()
				},
				success : function(d) {
					if (d.success == true) {

						notifySuccess(d.message);
					} else {
						notifyError(d.message);
					}

				}
			});
		}

		 function listCompany() {
			$.ajax({
				type : "POST",
				url : contextName + "/company/list.json",
				success : function(d) {
					if (d.success == true) {
						var dataSet = d.companyCollection
						$('#example').DataTable({
							data : dataSet,
							search : false,
							
							columns : [ {
								title: 'Company Code',
								data : 'code',
							}, {
								title: 'Company Name',
								data : 'name'
							}, {
								title: 'Created Date',
								data : 'createdDate'
							}, {
								title: 'Created By',
								data : 'createdBy',
							}, {
								title: 'Email',
								data : 'email',
								visible: false,
							},{
								title: 'Address',
								data : 'address',
								visible: false,
							},{
								title: 'Phone',
								data : 'phone',
								visible: false,
							},{				
								title: 'Action',
				                data: null,
				                className: "center",
				                defaultContent: '<a class="fa fa-pencil" data-toggle="modal" onclick=renderAction(this) id="ubah" class="editor_edit"></a> / <a class="fa fa-trash-o" href=""  class="editor_remove"></a> / <a data-toggle="modal" onclick=view(this) id="lihat" class="fa fa-search view"></a>'
				            }]
						});
					}
				}
				});
			
			}			
		 //code generated untuk di ADD button
		 function formCode() {
			 $.ajax({
			 	type	: "POST",
			 	url		: contextName + "/company/code.json",
			 	success	: function(d) {
			 		if(d.success == true){
			 			$('#companyCode').val(d.message);
			 		} else {
			 			notifyError(d.message);
					}
					
				}
			 })
		}
		 
		//edit button code
		 function renderAction(element){
			 var tableId	= $(element).closest('table').attr('id');
				var tr		= $(element).closest('tr');
				var data	= $('#'+tableId).DataTable().row(tr).data();
				$('#companyCode').val(data.code);
				$('#companyName').val(data.name);
				$('#email').val(data.email);
				$('#address').val(data.address);
				$('#phone').val(data.phone);
			
				$('#addModal').modal("show");
		 }
		
		//view button code
		 function view(element){
			 var tableId	= $(element).closest('table').attr('id');
				var tr		= $(element).closest('tr');
				var data	= $('#'+tableId).DataTable().row(tr).data();
				$('#companyCode').val(data.code).attr("readonly", true);
				$('#companyName').val(data.name).attr("readonly", true);
				$('#email').val(data.email).attr("readonly", true);
				$('#address').val(data.address).attr("readonly", true);
				$('#phone').val(data.phone).attr("readonly", true);
				
				$('#addModal').modal("show");
		 }
		
		 
		 
		 
	</script>