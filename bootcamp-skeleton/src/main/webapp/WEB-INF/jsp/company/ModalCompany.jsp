<!-- ADD Modal -->
		<div class="modal fade" id="addModal" role="dialog"
		data-parsley-validate>
			<div class="modal-dialog">
			
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Form Company</h4>
							</div>
							<div class="modal-body">
								<form class="form-horizontal">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Company Code</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="companyCode"
														placeholder="Auto Generated" name="companyCode"
														readonly="readonly">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Company Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="companyName"
														placeholder="Company Name" name="companyName"
														required="required">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Email</p>
												<div class="col-md-8">
													<input type="email" class="form-control" id="email"
														placeholder="Type email" name="email"
														required="required">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Address</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="address"
														placeholder="Type address" name="address">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Phone</p>
												<div class="col-md-8">
													<input type="tel" class="form-control" id="phone"
														placeholder="Type Last Name" name="phone">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-info" id="btnSave">Save</button>
								<button type="button" class="btn btn-warning" id="btnCancel"
									data-dismiss="modal">Cancel</button>
							</div>
						</div>

					</div>
				</div>