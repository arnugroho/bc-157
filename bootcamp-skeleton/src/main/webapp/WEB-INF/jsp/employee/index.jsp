<style>
<!--
input::placeholder {
	font-style: italic;
}
-->
</style>
<div class="row">
	<div class="col-xs-12">
		<!-- /.box-header -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">List Employee</h3>
			</div>
			<!-- body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<table>
							<button type="button" class="btn btn-info pull-right"
								data-toggle="modal" data-target="#addModal">Add</button>
						</table>
					</div>
				</div>
				<br>
				<!--Untuk tambah data  -->
				<!-- Modal -->
				<div class="modal fade" id="addModal" role="dialog">
					<div class="modal-dialog modal-lg">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Add Employee</h4>
							</div>
							<div class="modal-body">
								<form class="form-horizontal">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Emp ID Number</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="code"
														placeholder="Auto Generated" name="code"
														readonly="readonly">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Company Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="mCompanyId"
														placeholder="-Select Company Name-" name="mCompanyId"
														required="required">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*First Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="firstName"
														placeholder="Type Fisrt Name" name="firstName"
														required="required">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Email</p>
												<div class="col-md-8">
													<input type="email" class="form-control" id="email"
														placeholder="Type Email" name="email">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Last Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="lastName"
														placeholder="Type Last Name" name="lastName">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-info" id="btnSave">Save</button>
								<button type="button" class="btn btn-warning" id="btnCancel"
									data-dismiss="modal">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- Untuk modal view data -->
				<!-- Modal -->
				<div class="modal fade" id="viewModal" role="dialog">
					<div class="modal-dialog modal-lg">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">View Employee</h4>
							</div>
							<div class="modal-body">
								<form class="form-horizontal">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Emp ID Number</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="code"
														placeholder="Emp ID Number" name="code"
														readonly="readonly">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Company Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="mCompanyId"
														placeholder="Company Name" name="mCompanyId"
														readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*First Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="firstName"
														placeholder="Fisrt Name" name="firstName"
														readonly="readonly">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Email</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="email"
														placeholder="Email" name="email" readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Last Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="lastName"
														placeholder="Last Name" name="lastName"
														readonly="readonly">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" id="btnClose"
									data-dismiss="modal">Close</button>
							</div>
						</div>

					</div>
				</div>
				<!-- Untuk modal edit data -->
				<!-- Modal -->
				<div class="modal fade" id="editModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit Employee</h4>
							</div>
							<div class="modal-body">
								<form class="form-horizontal">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Emp ID Number</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="code"
														placeholder="Emp ID Number" name="code"
														readonly="readonly" value="">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*Company Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="mCompanyId"
														name="mCompanyId" required="required" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">*First Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="firstName"
														value="" name="firstName" required="required"">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Email</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="email"
														name="email" value="">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<p class="col-md-4">Last Name</p>
												<div class="col-md-8">
													<input type="text" class="form-control" id="lastName"
														value="" name="lastName">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-info" id="btnUpdate">Update</button>
								<button type="button" class="btn btn-warning" id="btnCancel"
									data-dismiss="modal">Cancel</button>
							</div>
						</div>

					</div>
				</div>
				<!-- Untuk modal delete data -->
				<!-- Modal -->
				<div class="modal fade" id="deleteModal" role="dialog">
					<div class="modal-dialog modal-sm">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header" id="modaldelete">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Delete Data ?</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<button type="button" class="btn btn-info" id="btnDelete">Delete</button>
										<button type="button" class="btn btn-warning" id="btnCancel"
											data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- form start -->
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3">
							<div class="container-fluid">
								<div class="form-group">
									<div class="input-group date">
										<input type="text" class="form-control" id="datepicker"
											placeholder="Created">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="input-group date">
									<input type="text" class="form-control" id="searchCreatedBy"
										placeholder="Created by:">
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="button" class="btn btn-warning" id="btnSearch">Search</button>
						</div>
					</div>
				</div>
				<table id="example" class="stripe" style="width: 100%">
				</table>
				<!-- <form class="form-inline" style="padding-top: 30px">
					<div class="form-group ">
						<div class="col-sm-5">
							<input list="code" name="code"
								placeholder="- Select Company Code -">
							<datalist id="code">
								<option value="Internet Explorer"></option>
								<option value="Firefox"></option>
								<option value="Chrome"></option>
								<option value="Opera"></option>
								<option value="Safari"></option>
							</datalist>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-5">
							<input list="name" name="name"
								placeholder="- Select Company Name -">
							<datalist id="name">
								<datalist id="name">
									<option value="Internet Explorer"></option>
									<option value="Firefox"></option>
									<option value="Chrome"></option>
									<option value="Opera"></option>
									<option value="Safari"></option>
								</datalist>
							</datalist>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-5">
							<input type="date" name="date" placeholder="- Date -"> </input>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-5">
							<input type="text" name="createdBy" placeholder="Created By">
							</input>
						</div>
					</div>

					<div class="form-group">

						<div class="col-sm-5">
							
						</div>
					</div>
				</form> -->
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		listEmployee();
		$('#datepicker').datepicker({
			format : 'yyyy-mm-dd',
			autoclose : true
		})
		$('#btnSave').click(function() {
			insertEmployee();

		});

	})

	function insertEmployee() {
		$.ajax({
			type : "POST",
			url : contextName + "/employee/insert.json",
			data : {
				code : $('#code').val(),
				firstName : $('#firstName').val(),
				lastName : $('#lastName').val(),
				mCompanyId : $('#mCompanyId').val(),
				email : $('#email').val()
			},
			success : function(d) {
				if (d.success == true) {

					notifySuccess(d.message);
				} else {
					notifyError(d.message);
				}

			}
		});
	}

	function listEmployee() {
		$
				.ajax({
					type : "POST",
					url : contextName + "/employee/list.json",
					success : function(d) {
						if (d.success == true) {
							var dataSet = d.employeeCollection;
							$('#example')
									.DataTable(
											{
												searching : false,
												data : dataSet,
												columns : [
														{
															title : 'No',
															data : 'id'
														},
														{
															title : 'Employee ID Number',
															data : 'code'
														},
														{
															title : 'Employee Name',
															data : null,
															render : function(
																	data, type,
																	row) {
																return data.firstName
																		+ ' '
																		+ data.lastName;
															}
														},
														{
															title : 'Company Name',
															data : 'mCompanyId'
														},
														{
															title : 'Created Date',
															data : 'createdDate'
														},
														{
															title : 'Created By',
															data : 'createdBy'
														},
														{
															title : 'Action',
															data : null,
															className : "center",
															defaultContent : '<a href="#" data-toggle="modal" data-target="#viewModal" class="editor_search"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="#" data-toggle="modal" data-target="#editModal" class="editor_edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a href="#" data-toggle="modal" data-target="#deleteModal" class="editor_remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
														} ]
											});
							notifySuccess(d.message);
						} else {
							notifyError(d.message);
						}

					}
				});
	}
</script>