<div class="col-md-12">
	<!-- Horizontal Form -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Form Buku</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal">
			<div class="box-body">
				<div class="form-group">
					<label for="kodeBuku" class="col-sm-2 control-label">Kode
						Buku</label>

					<div class="col-sm-10">
						<input class="form-control" id="kodeBuku" placeholder="Kode Buku"
							type="text">
					</div>
				</div>
				<div class="form-group">
					<label for="namaBuku" class="col-sm-2 control-label">Nama
						Buku</label>

					<div class="col-sm-10">
						<input class="form-control" id="namaBuku" placeholder="Nama Buku"
							type="text">
					</div>
				</div>

			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<!-- <button type="submit" class="btn btn-default">Cancel</button> -->
				<button type="button" class="btn btn-info pull-right" id="btnSubmit">Submit</button>
			</div>
			<!-- /.box-footer -->
		</form>
		<table id="example" class="stripe"></table>
	</div>
	<!-- /.box -->

	<!-- /.box -->

</div>

<script>
	$(function() {
		listBuku();
		$('#btnSubmit').click(function() {
			insertBuku();

		});

// 		 $('#example').DataTable( {
// 		    data: dataSet,
// 		    columns: [
// 		        { data: 'name' },
// 		        { data: 'position' },
// 		        { data: 'salary' },
// 		        { data: 'office' }
// 		    ]
// 		} ); 

	})

	function insertBuku() {
		$.ajax({
			type : "POST",
			url : contextName + "/buku/insert.json",
			data : {
				kodeBuku : $('#kodeBuku').val(),
				namaBuku : $('#namaBuku').val()
			},
			success : function(d) {
				if (d.success == true) {

					notifySuccess(d.message);
				} else {
					notifyError(d.message);
				}

			}
		});
	}

	function listBuku() {
		$.ajax({
			type : "POST",
			url : contextName + "/buku/list.json",
			success : function(d) {
				if (d.success == true) {
					var dataSet = d.bukuCollection;
					console.log(dataSet + "x")
					$('#example').DataTable({
						data : dataSet,
						columns : [ {
							data : 'kodeBuku'
						}, {
							data : 'namaBuku'
						} ]
					});
					notifySuccess(d.message);
				} else {
					notifyError(d.message);
				}

			}
		});
	}
</script>