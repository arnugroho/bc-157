package com.xsis.bootcamp.controller;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.Buku;
import com.xsis.bootcamp.model.Personel;
import com.xsis.bootcamp.service.BukuService;

@Controller
@RequestMapping(value = "/buku")
public class BukuController extends BaseController {
	@Autowired
	private BukuService bukuService;

	@RequestMapping(value = "/index")
	public String index() {
		return "buku/index";
	}

	@RequestMapping(value = "/insert")
	public String insert(Model model, HttpServletRequest req) {

		try {
			Personel user = getUser();
			Date currentDate = new Date();
			String namaBuku = req.getParameter("namaBuku");
			String kodeBuku = req.getParameter("kodeBuku");
			Buku buku = new Buku();
			buku.setNamaBuku(namaBuku);
			buku.setKodeBuku(kodeBuku);
			buku.setCreatedBy(user.getId());
			buku.setCreatedOn(currentDate);
			bukuService.insert(buku);

			model.addAttribute("success", true);
			model.addAttribute("message", "Data Berhasil Di insert");

		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Data gagal Di insert");

			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/list")
	public String listData(Model model) {

		try {
			Collection<Buku> bukuCollection = bukuService.listAll();
			model.addAttribute("bukuCollection", bukuCollection);
			model.addAttribute("success", true);
			model.addAttribute("message", "Data Berhasil Di ambil");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Data gagal diambil");
			e.printStackTrace();
		}
		return null;
	}

}
