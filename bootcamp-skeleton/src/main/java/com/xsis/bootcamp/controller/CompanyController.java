package com.xsis.bootcamp.controller;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.Company;
import com.xsis.bootcamp.model.Personel;
import com.xsis.bootcamp.service.CompanyService;

@Controller
@RequestMapping(value="/company")
public class CompanyController extends BaseController{
	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value="/indexcompany")
	public String indexCompany() {
		return "company/IndexCompany";
		
	}
	
	@RequestMapping(value="/insert")
	public String insert(Model model, HttpServletRequest req) {
		
		try {
			Personel user = getUser();
			Date currentDate = new Date();
			String companyCode = req.getParameter("companyCode");
			String companyName = req.getParameter("companyName");
			String email = req.getParameter("email");
			String phone = req.getParameter("phone");
			String address = req.getParameter("address");
			Company company = new Company();
			company.setCode(companyCode);
			company.setName(companyName);
			company.setEmail(email);
			company.setPhone(phone);
			company.setAddress(address);
			company.setIsDelete(false);
			company.setCreatedDate(currentDate);
			company.setCreatedBy(user.getUsername());
			company.setUpdatedBy(user.getUsername());
			company.setUpdatedDate(currentDate);
			companyService.insert(company);
			
			model.addAttribute("success", true);
			model.addAttribute("message", "Data Company Berhasil di Insert");

		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Data Company Gagal di Insert");
		
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	@RequestMapping(value="/list")
	public String listData(Model model) {
		
		try {
			Collection<Company> companyCollection= companyService.listAll();
			model.addAttribute("companyCollection", companyCollection);
			model.addAttribute("success", true);
			model.addAttribute("message", "Data Company Berhasil diambil");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Data Company gagal diambil");
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/code")
	public String listCode(Model model) {
		try {
			String code = companyService.getCode();
		
			model.addAttribute("success", true);
			model.addAttribute("message", code );
			
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("success", false);
			model.addAttribute("message", "Kode gagal");
		}
		return null;
		
	}

}
