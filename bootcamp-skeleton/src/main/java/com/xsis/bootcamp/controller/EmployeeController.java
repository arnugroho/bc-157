package com.xsis.bootcamp.controller;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.bootcamp.model.Employee;
import com.xsis.bootcamp.model.Personel;
import com.xsis.bootcamp.service.EmployeeService;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController extends BaseController {
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/index")
	public String index() {
		return "employee/index";
	}

	@RequestMapping(value = "/insert")
	public String insert(Model model, HttpServletRequest request) {
		try {
			Personel user = getUser();
			Date currentDate = new Date();
			String code = employeeService.getCode();
			String fisrtName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String mCompanyId = request.getParameter("mCompanyId");
			String email = request.getParameter("email");
			Employee employee = new Employee();
			employee.setCode(code);
			employee.setFirstName(fisrtName);
			employee.setLastName(lastName);
			employee.setmCompanyId(Integer.parseInt(mCompanyId));
			employee.setEmail(email);
			employee.setCreatedBy(user.getUsername());
			employee.setCreatedDate(currentDate);
//			if(employee.getIsDelete().equals(false)) {
			employee.setIsDelete(false);
//			} else {
//				employee.setIsDelete(Boolean.valueOf("1"));
//			}
			employeeService.insert(employee);
			model.addAttribute("success", true);
			model.addAttribute("message", "Insert data Employee berhasil");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Insert data Employee gagal");
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/list")
	public String listData(Model model) {

		try {
			Collection<Employee> employeeCollection = employeeService.listAll();
			model.addAttribute("employeeCollection", employeeCollection);
			model.addAttribute("success", true);
			model.addAttribute("message", "Data Employee berhasil diambil");
		} catch (Exception e) {
			model.addAttribute("success", false);
			model.addAttribute("message", "Data Employee gagal diambil");
			e.printStackTrace();
		}
		return null;
	}
}
