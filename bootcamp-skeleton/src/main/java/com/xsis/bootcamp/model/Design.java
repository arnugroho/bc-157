package com.xsis.bootcamp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = Design.TABLE_NAME)
public class Design {
	public static final String TABLE_NAME = "t_design";
	@Id
	@Column(name = "id", length = 11, nullable = true)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
	@TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	private Integer id;
	@Column(name = "code", length = 50, nullable = false)
	private String code;
	//ini belum dibikin join, tabelnya belom ada
	@Column(name = "t_event_id", length = 11, nullable = false)
	private Integer tEventId;
	@Column(name = "title_header", length = 255)
	private String titleHeader;
	@Column(name = "request_by", length = 11, nullable = false)
	private Integer requestBy;
	@Temporal(TemporalType.DATE)
	@Column(name = "request_date", nullable = false)
	private Date requestDate;
	@Column(name = "approved_by", length = 11)
	private Integer approvedBy;
	@Temporal(TemporalType.DATE)
	@Column(name = "approved_date")
	private Date approvedDate;
	@Column(name = "assign_to", length = 50)
	private Integer assignTo;
	@Temporal(TemporalType.DATE)
	@Column(name = "closed_date")
	private Date closedDate;
	@Column(name = "note", length = 255)
	private String note;
	@Column(name = "status", length = 1)
	private Integer status;
	@Column(name = "reject_reason", length = 255)
	private String rejectReason;
	@Column(name = "is_delete", nullable = false)
	private Boolean isDelete;
	@Column(name = "created_by", length = 50, nullable = false)
	private String createdBy;
	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", nullable = false)
	private Date createdDate;
	@Column(name = "updated_by", length = 50)
	private String updatedBy;
	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date")
	private Date updatedDate;
	@ManyToOne
	@JoinColumn(name = "request_by", updatable = false, insertable = false)
	private Employee requestByDesc;
	@ManyToOne
	@JoinColumn(name = "approved_by", updatable = false, insertable = false)
	private Employee approvedByDesc;
	@ManyToOne
	@JoinColumn(name = "assign_to", updatable = false, insertable = false)
	private Employee assignToDesc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer gettEventId() {
		return tEventId;
	}

	public void settEventId(Integer tEventId) {
		this.tEventId = tEventId;
	}

	public String getTitleHeader() {
		return titleHeader;
	}

	public void setTitleHeader(String titleHeader) {
		this.titleHeader = titleHeader;
	}

	public Integer getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(Integer requestBy) {
		this.requestBy = requestBy;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Integer getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Integer getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}

	public Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Employee getRequestByDesc() {
		return requestByDesc;
	}

	public void setRequestByDesc(Employee requestByDesc) {
		this.requestByDesc = requestByDesc;
	}

	public Employee getApprovedByDesc() {
		return approvedByDesc;
	}

	public void setApprovedByDesc(Employee approvedByDesc) {
		this.approvedByDesc = approvedByDesc;
	}

	public Employee getAssignToDesc() {
		return assignToDesc;
	}

	public void setAssignToDesc(Employee assignToDesc) {
		this.assignToDesc = assignToDesc;
	}

}
