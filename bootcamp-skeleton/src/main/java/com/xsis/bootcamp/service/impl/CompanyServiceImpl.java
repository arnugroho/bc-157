package com.xsis.bootcamp.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.CompanyDao;
import com.xsis.bootcamp.model.Company;
import com.xsis.bootcamp.service.CompanyService;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyDao companyDao;
	
	private String generateCode;
	
	@Override
	public void insert(Company company) throws Exception {
		companyDao.insert(company);
		
	}

	@Override
	public void update(Company company) throws Exception {
		companyDao.update(company);
		
	}

	@Override
	public void delete(Company company) throws Exception {
		companyDao.delete(company);
		
	}

	@Override
	public Collection<Company> listAll() throws Exception {
	
		return companyDao.listAll();
	}

	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		Integer codeNumber = Integer.parseInt(companyDao.getCode());
		if (codeNumber < 10) {
			generateCode = "CP000"+String.valueOf(codeNumber);
		} else if (codeNumber >= 10 && codeNumber < 100) {
			generateCode = "CP00"+String.valueOf(codeNumber);
		} else if (codeNumber >= 100 && codeNumber < 1000) {
			generateCode = "CP0"+String.valueOf(codeNumber);
		} else {
			generateCode = "CP"+String.valueOf(codeNumber);
		}
		return generateCode;
	}

}
