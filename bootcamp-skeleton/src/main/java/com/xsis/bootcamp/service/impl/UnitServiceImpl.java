package com.xsis.bootcamp.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.UnitDao;
import com.xsis.bootcamp.model.Unit;
import com.xsis.bootcamp.service.UnitService;

@Service
@Transactional
public class UnitServiceImpl implements UnitService{
	
	@Autowired
	private UnitDao unitDao;
	
	private String generateCode;

	@Override
	public void insert(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		unitDao.insert(unit);
	}

	@Override
	public void update(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		unitDao.update(unit);
	}

	@Override
	public void delete(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		unitDao.delete(unit);
	}

	@Override
	public Collection<Unit> listAll() throws Exception {
		// TODO Auto-generated method stub
		return unitDao.listAll();
	}

	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		Integer codeNumber = Integer.parseInt(unitDao.getCode());
		if (codeNumber < 10) {
			generateCode = "UN000"+String.valueOf(codeNumber);
		} else if (codeNumber >= 10 && codeNumber < 100) {
			generateCode = "UN00"+String.valueOf(codeNumber);
		} else if (codeNumber >= 100 && codeNumber < 1000) {
			generateCode = "UN0"+String.valueOf(codeNumber);
		} else {
			generateCode = "UN"+String.valueOf(codeNumber);
		}
		return generateCode;
	}

	@Override
	public Unit getUnit(String code) throws Exception {
		// TODO Auto-generated method stub
		return unitDao.getUnit(code);
	}

}
