package com.xsis.bootcamp.service;

import java.util.Collection;

import com.xsis.bootcamp.model.Buku;

public interface BukuService {
	public abstract void insert(Buku buku) throws Exception;

	public abstract void update(Buku buku) throws Exception;

	public abstract void delete(Buku buku) throws Exception;
	
	public abstract Collection<Buku> listAll() throws Exception;
}
