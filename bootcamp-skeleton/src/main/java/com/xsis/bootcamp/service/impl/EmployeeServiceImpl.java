package com.xsis.bootcamp.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.EmployeeDao;
import com.xsis.bootcamp.model.Employee;
import com.xsis.bootcamp.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	private String generateCode;
	private DateFormat df = new SimpleDateFormat("yy-MM-dd");

	@Override
	public Collection<Employee> listAll() throws Exception {
		return employeeDao.listAll();
	}

	@Override
	public void insert(Employee employee) throws Exception {
		employeeDao.insert(employee);
	}

	@Override
	public void update(Employee employee) throws Exception {
		employeeDao.update(employee);

	}

	@Override
	public void delete(Employee employee) throws Exception {
		employeeDao.delete(employee);

	}

	@Override
	public String getCode() throws Exception {
		Integer codeNumber = Integer.parseInt(employeeDao.getCode());
		Date now = new Date();
		String date = df.format(now);
		String tahun = date.substring(0, 2);
		String bulan = date.substring(3, 5);
		String hari = date.substring(6, 8);
		if (codeNumber < 10) {
			generateCode = tahun + "." + bulan + "." + hari + "." + "0" + String.valueOf(codeNumber);
		} else {
			generateCode = tahun + "." + bulan + "." + hari + "." + String.valueOf(codeNumber);
		}
		return generateCode;
	}

	@Override
	public Employee getEmployee(String employee_number) throws Exception {
		// TODO Auto-generated method stub
		return employeeDao.getEmployee(employee_number);
	}

}
