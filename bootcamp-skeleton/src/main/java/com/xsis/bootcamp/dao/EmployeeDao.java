package com.xsis.bootcamp.dao;

import java.util.Collection;

import com.xsis.bootcamp.model.Employee;

public interface EmployeeDao {
	public Collection<Employee> listAll() throws Exception;

	public void insert(Employee employee) throws Exception;

	public void update(Employee employee) throws Exception;

	public void delete(Employee employee) throws Exception;

	public String getCode() throws Exception;

	public Employee getEmployee(String employee_number) throws Exception;
}
