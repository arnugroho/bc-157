package com.xsis.bootcamp.dao;

import java.util.Collection;

import com.xsis.bootcamp.model.Company;

public interface CompanyDao {
	public void insert(Company company) throws Exception;

	public void update(Company company) throws Exception;

	public void delete(Company company) throws Exception;

	public Collection<Company> listAll() throws Exception;

	public String getCode() throws Exception;
	
}
