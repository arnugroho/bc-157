package com.xsis.bootcamp.dao.impl;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.CompanyDao;
import com.xsis.bootcamp.model.Company;
import com.xsis.bootcamp.util.SessionHibernate;

@Repository
public class CompanyDaoImpl extends SessionHibernate implements CompanyDao{
	
	public void insert(Company company) throws Exception {
		getSession().save(company);
		
	}

	public void update(Company company) throws Exception {
		getSession().update(company);
		
	}

	public void delete(Company company) throws Exception {
		getSession().delete(company);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Company> listAll() throws Exception {
		String query = " from Company where is_delete = 'f'";
		return getSession().createQuery(query).list();
	}

	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		Query q = getSession().createSQLQuery("select nextval('sequence_buku')");
		String k = q.uniqueResult().toString();
		return k;
	}



}
