package com.xsis.bootcamp.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.UnitDao;
import com.xsis.bootcamp.model.Unit;
import com.xsis.bootcamp.util.SessionHibernate;

@Repository
public class UnitDaoImpl extends SessionHibernate implements UnitDao{
	
	@Override
	public void insert(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		getSession().save(unit);
	}

	@Override
	public void update(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		getSession().update(unit);
	}

	@Override
	public void delete(Unit unit) throws Exception {
		// TODO Auto-generated method stub
		getSession().delete(unit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Unit> listAll() throws Exception {
		// TODO Auto-generated method stub
		
		String query = "from Unit where is_delete = 'f'";
		return getSession().createQuery(query).list();
	}

	
	@Override
	public String getCode() throws Exception {
		// TODO Auto-generated method stub
		//Query query = "select nextval('sequence_buku')";
		/*String query = "select sequence_buku.nextval";
		List result = (List) getSession().createQuery(query);
		return result.toString();*/
		Query q = getSession().createSQLQuery("select nextval('sequence_buku')");
		String k  = q.uniqueResult().toString();
		return k;
	}

	@Override
	public Unit getUnit(String code) throws Exception {
		// TODO Auto-generated method stub
		String hql = "from Unit where code = :code";
		Query query = getSession().createQuery(hql);
		query.setString("code", code);
		if (query.list().size() > 0) {
			return (Unit) query.list().get(0);
		} else {
			return null;
		}
	}

}
