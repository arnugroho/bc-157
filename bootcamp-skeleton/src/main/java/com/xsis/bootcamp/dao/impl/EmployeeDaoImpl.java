package com.xsis.bootcamp.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.EmployeeDao;
import com.xsis.bootcamp.model.Employee;
import com.xsis.bootcamp.util.SessionHibernate;

@Repository
public class EmployeeDaoImpl extends SessionHibernate implements EmployeeDao {

	@SuppressWarnings("unchecked")
	public Collection<Employee> listAll() throws Exception {
		String query = "from Employee";
		return getSession().createQuery(query).list();
	}

	@Override
	public void insert(Employee employee) throws Exception {
		getSession().save(employee);

	}

	@Override
	public void update(Employee employee) throws Exception {
		getSession().merge(employee);

	}

	@Override
	public void delete(Employee employee) throws Exception {
		getSession().delete(employee);

	}

	@Override
	public String getCode() throws Exception {
		Query q = getSession().createSQLQuery("select nextval('sequence_buku')");
		String k  = q.uniqueResult().toString();
		return k;
	}

	@Override
	public Employee getEmployee(String employee_number) throws Exception {
		String hql = "from Employee where employee_number = :employee_number";
		Query query = getSession().createQuery(hql);
		query.setString("employee_number", employee_number);
		if (query.list().size() > 0) {
			return (Employee) query.list().get(0);
		} else {
			return null;
		}
	}

}
